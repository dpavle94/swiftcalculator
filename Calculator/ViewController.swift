//
//  ViewController.swift
//  Calculator
//
//  Created by Daniel Pavlekovic on 30.03.2017..
//  Copyright © 2017. Daniel Pavlekovic. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var display: UILabel!
    
    var userIsInTheMiddleOfTyping = false
    var userHasTypedAFloatingPoint = false
    
    
    @IBAction func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        if(digit=="."){
            if((display.text!.range(of: ".")) == nil){
                let textCurrentlyInTheDisplay = display.text!
                display.text=textCurrentlyInTheDisplay + digit
                userIsInTheMiddleOfTyping=true
            }
        }
        else{
            if userIsInTheMiddleOfTyping{
                let textCurrentlyInTheDisplay = display.text!
                display.text = textCurrentlyInTheDisplay + digit
            }
            else{
                display.text=digit
                userIsInTheMiddleOfTyping=true
            }
        }
    }
    
    var displayValue: Double{
        get{
            return Double(display.text!)!
        }
        set{
            display.text = display_formatter(newValue)
        }
    }
    
    private var brain = CalculatorBrain()
    
    @IBAction func performOperation(_ sender: UIButton) {
        if userIsInTheMiddleOfTyping{
            brain.setOperand(displayValue)
            userIsInTheMiddleOfTyping = false
        }
        if let mathematicalSymbol = sender.currentTitle{
            brain.performOperation(mathematicalSymbol)
        }
        if let result = brain.result {
            displayValue = result
            
        }
    }
    
    func display_formatter(_ value: Double) -> String{
        let nm_format = NumberFormatter()
        nm_format.maximumFractionDigits = 6
        nm_format.numberStyle = .decimal
        let result = nm_format.string(from: value as NSNumber)
        return result!
    }
    
    @IBAction func touchClear(_ sender: UIButton) {
        userIsInTheMiddleOfTyping=false
        userHasTypedAFloatingPoint=false
        brain.setOperand(0)
        brain.performOperation("*")
        displayValue = 0
    }
    

}

